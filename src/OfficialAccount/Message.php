<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\OfficialAccount;

/**
 * @property string $Event
 * @property string $MsgType
 */
class Message extends \QianSionEasyWeChat\Kernel\Message
{
    //
}
