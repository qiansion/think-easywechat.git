<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\OpenPlatform\Contracts;

interface VerifyTicket
{
    public function getTicket(): string;
}
