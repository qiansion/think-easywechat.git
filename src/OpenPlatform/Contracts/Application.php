<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\OpenPlatform\Contracts;

use QianSionEasyWeChat\Kernel\Contracts\AccessToken;
use QianSionEasyWeChat\Kernel\Contracts\Config;
use QianSionEasyWeChat\Kernel\Contracts\Server;
use QianSionEasyWeChat\Kernel\Encryptor;
use QianSionEasyWeChat\Kernel\HttpClient\AccessTokenAwareClient;
use QianSionEasyWeChat\MiniApp\Application as MiniAppApplication;
use QianSionEasyWeChat\OfficialAccount\Application as OfficialAccountApplication;
use QianSionEasyWeChat\OpenPlatform\AuthorizerAccessToken;
use Overtrue\Socialite\Contracts\ProviderInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\SimpleCache\CacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

interface Application
{
    public function getAccount(): Account;

    public function getEncryptor(): Encryptor;

    public function getServer(): Server;

    public function getRequest(): ServerRequestInterface;

    public function getClient(): AccessTokenAwareClient;

    public function getHttpClient(): HttpClientInterface;

    public function getConfig(): Config;

    public function getComponentAccessToken(): AccessToken;

    public function getCache(): CacheInterface;

    public function getOAuth(): ProviderInterface;

    /**
     * @param  array<string, mixed>  $config
     */
    public function getMiniApp(AuthorizerAccessToken $authorizerAccessToken, array $config): MiniAppApplication;

    /**
     * @param  array<string, mixed>  $config
     */
    public function getOfficialAccount(
        AuthorizerAccessToken $authorizerAccessToken,
        array $config
    ): OfficialAccountApplication;
}
