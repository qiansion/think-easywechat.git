<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\Pay\Contracts;

use Psr\Http\Message\MessageInterface;

interface Validator
{
    /**
     * @throws \QianSionEasyWeChat\Pay\Exceptions\InvalidSignatureException if signature validate failed.
     */
    public function validate(MessageInterface $message): void;
}
