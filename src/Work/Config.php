<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\Work;

class Config extends \QianSionEasyWeChat\Kernel\Config
{
    /**
     * @var array<string>
     */
    protected array $requiredKeys = [
        'corp_id',
        'secret',
        'token',
        'aes_key',
    ];
}
