<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\OpenWork;

use ArrayAccess;
use QianSionEasyWeChat\Kernel\Contracts\Arrayable;
use QianSionEasyWeChat\Kernel\Contracts\Jsonable;
use QianSionEasyWeChat\Kernel\Traits\HasAttributes;

/**
 * @implements ArrayAccess<string, mixed>
 */
class Authorization implements ArrayAccess, Jsonable, Arrayable
{
    use HasAttributes;

    public function getAppId(): string
    {
        /** @phpstan-ignore-next-line */
        return (string) $this->attributes['auth_corp_info']['corpid'];
    }
}
