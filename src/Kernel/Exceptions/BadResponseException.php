<?php

declare(strict_types=1);

namespace QianSionEasyWeChat\Kernel\Exceptions;

class BadResponseException extends Exception
{
}
